
function showHideDescription(){
    $(this).find(".description").toggle();
}
 
$(document).ready(function(){
    $('.pizza-type label').hover(function(){
        $(this).find(".description").show();
    },
    function(){
        $(this).find(".description").hide();
    },
    );
 
	function updateprix() {
		
        let monprix = 0;

        monprix += +$("input[name='type']:checked").data("price") || 0;
		
        monprix += +$("input[name='pate']:checked").data("price") || 0;
		
        $("input[name='extra']:checked").each(function(i, elem) {
			
            monprix += $(elem).data("price");
			
        });

        monprix *= $(".nb-parts input").val() / 6;

        $(".stick-right p").text(monprix + " €");
    }
	
	$("input[data-price]").change(updateprix);
	
 
    $('.nb-parts input').on('keyup', function() {
        $(".pizza-pict").remove();
		
        var pizza = $('<span class="pizza-pict"></span>');
        slices = +$(this).val();
 
        for(i = 0; i < slices/6 ; i++) {
            $(this).after(pizza.clone().addClass('pizza-6'));
        }
 
        if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);
    });


    $(".next-step" ).click( function() {
        $(this).remove();
        $(".infos-client").slideDown();
    });
 
    $(".add" ).click( function() {
        
		$(".infos-client .type:nth-child(2) input").append("<input type='text'/>");

        $(".infos-client .add").before("<br /><input type='text'/>");
		
    });
	
	$(".done" ).click( function() {
		const prenom = $('.infos-client .type:first input').val();
		
        $(".main")
			.empty()
			.append("<p>Merci " + prenom + " ! Votre commande sera livrée dans 15 minutes</p>");
    });
	

});